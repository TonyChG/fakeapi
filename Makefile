.PHONY: help
NAME=fakeapi
VENV_NAME?=venv
VENV_ACTIVATE=. $(VENV_NAME)/bin/activate
PYTHON=${VENV_NAME}/bin/python3


.DEFAULT: help
help:
	@echo "make venv"
	@echo "       Simply do source venv/bin/activate to get the environemnt"
	@echo "make build"
	@echo "       Build last release using setup.py"
	@echo "make install"
	@echo "       Install the package"
	@echo "make clean"
	@echo "       Clean package"
	@echo "make fclean"
	@echo "       Clean package and virtualenv"
	@echo "make repository"
	@echo "       Copy the release to repository"


all: fclean build push


venv: $(VENV_NAME)/bin/activate
$(VENV_NAME)/bin/activate: setup.py
	test -d $(VENV_NAME) || virtualenv -p python3 $(VENV_NAME)
	${PYTHON} -m pip install -U pip
	touch $(VENV_NAME)/bin/activate


install:        venv
	${PYTHON} setup.py install

build:          venv
	${PYTHON} setup.py sdist bdist_wheel

repository:     build
	twine upload --repository gitlab dist/*

clean:
	rm -vfr build/* dist/* **/*.egg-info *.egg-info

fclean:         clean
	rm -vfr $(VENV_NAME)
	rm -vfr public/*

