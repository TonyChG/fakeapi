# FakeAPI

## Installation

```bash
pip install fakeapi --extra-index-url https://gitlab.com/api/v4/projects/19794555/packages/pypi/simple
```

Run a FakeAPI using a YAML configuration as input

```yaml
routes:
  - name: projects                        # Name of the groups of routes
    endpoint: /api/projects/<project>     # The API endpoint where the routes will be binded
    data:                                 # Data serve by the API (JSON)
      project[1:10]:                      # Range of project (project1, project2, ..., project10)
        id: rand:::int                    # Randomize the ID
        name: <project>                   # Template of the key (project1 for example)
        fqdn: <project>.example.com
  - name: servers
    endpoint: /api/servers/<server>
    data:
      server1:                            # You can assign data to endpoint statically
        id: rand:::int
        name: super_server1
        init: static_init
      server2:
        id: rand:::int
        name: super_server2
        init: static_init
```

## Launch the script

```bash
$ FAKEAPI_CONFIG=https://gitlab.com/TonyChG/fakeapi/-/raw/master/samples/simple.yml gunicorn -w 4 fakeapi.web:app
[2020-07-09 15:06:39 +0200] [146864] [INFO] Starting gunicorn 20.0.4
[2020-07-09 15:06:39 +0200] [146864] [INFO] Listening at: http://127.0.0.1:8000 (146864)
[2020-07-09 15:06:39 +0200] [146864] [INFO] Using worker: sync
[2020-07-09 15:06:39 +0200] [146868] [INFO] Booting worker with pid: 146868
[2020-07-09 15:06:39 +0200] [146869] [INFO] Booting worker with pid: 146869
[2020-07-09 15:06:39 +0200] [146870] [INFO] Booting worker with pid: 146870
[2020-07-09 15:06:39 +0200] [146871] [INFO] Booting worker with pid: 146871
```

## Get the results

```
$ curl http://127.0.0.1:8000/api/projects/project1
{
    "id": 33,
    "name": "project1",
    "fqdn": "project1.example.com"
}
```
