import re


def search_ranges(key, data):
    for label, ranges in data.items():
        matchs = re.search(r"(\w+)\[(\d+):(\d+)\]", label)
        if matchs is not None:
            prefix = matchs.group(1)
            data_min = int(matchs.group(2))
            data_max = int(matchs.group(3))
            matchs = re.search(r'{}(\d+)'.format(prefix), key)
            if matchs is not None:
                input_id = int(matchs.group(1))
                if data_min <= input_id <= data_max:
                    return ranges


def search_rand(value):
    type_value = None

    if isinstance(value, str):
        matchs = re.search(r"^rand:::(\w+)", value)
        if matchs is not None:
            type_value = matchs.group(1)
    return type_value

