import re
import os
import sys
import time
import logging
import requests
import argparse
import hashlib
import subprocess

# from systemd import journal


logging.basicConfig()
log = logging.getLogger(__name__)
# log.addHandler(journal.JournaldLogHandler())
log.setLevel(logging.INFO)


def compute_sha1(data):
    sha1 = hashlib.sha1()
    sha1.update(data)
    return sha1.hexdigest()


def parse_cli_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("--config", type=str, default=None,
                        help="Configuration path")
    parser.add_argument("--interval", type=float, default=1,
                        help="Refresh rate in seconds")
    return parser.parse_args()


def reloading():
    log.info("Reloading fakeapi.service ...")
    p = subprocess.Popen(["systemctl", "restart", "fakeapi.service"])
    return p.wait()


def watch(args, config_path):
    try:
        last_checksum = None

        if config_path is not None:
            while True:
                if re.match("^(http|https).+", config_path):
                    res = requests.get(config_path)
                    if res.status_code != 200:
                        log.fatal("Invalid URL %s" % config_path)
                    else:
                        checksum = compute_sha1(res.content)
                else:
                    with open(config_path, 'rb') as f:
                        checksum = compute_sha1(f.read())
                if last_checksum is not None \
                        and last_checksum != checksum:
                    if reloading() != 0:
                        log.fatal("systemctl restart fakeapi.service failed")
                last_checksum = checksum
                time.sleep(args.interval)
    except KeyboardInterrupt:
        sys.exit(0)


def main():
    args = parse_cli_args()
    config_path = args.config or os.getenv("FAKEAPI_CONFIG")

    log.info("Configuration %s" % config_path)
    watch(args, config_path)


if __name__ == '__main__':
    main()
