import os
import re
import json
import logging
import argparse
import random
from io import StringIO

import yaml
import requests
from flask import Flask, Response


logging.basicConfig()
log = logging.getLogger()
log.setLevel(logging.INFO)

app = Flask(__name__)


class HandleResponse:
    def __init__(self, endpoint, name, data, param=None):
        self.data = data
        self.param = param
        self.__name__ = name
        self.endpoint = endpoint

    def get_random_value(self, seed, type_value):
        random.seed(seed)
        if type_value == "int":
            return random.randint(0, 1000)

    def get_random_object(self, key):
        obj = None
        ranges = self.search_ranges(key)

        if ranges is not None:
            obj = ranges.copy()
            for label, value in obj.items():
                type_value = self.search_rand(value)
                if type_value is not None:
                    obj[label] = self.get_random_value(key, type_value)
                if re.search(r'<{}>'.format(self.param), value) is not None:
                    obj[label] = value.replace('<{}>'.format(self.param), key)
        elif self.is_any_data(key):
            obj = self.data.get(key)

            for label, value in obj.items():
                type_value = self.search_rand(value)
                if type_value is not None:
                    obj[label] = self.get_random_value(key, type_value)
        return obj

    def search_ranges(self, key):
        for label, ranges in self.data.items():
            matchs = re.search("(\w+)\[(\d+):(\d+)\]", label)
            if matchs is not None:
                prefix = matchs.group(1)
                data_min = int(matchs.group(2))
                data_max = int(matchs.group(3))
                matchs = re.search(r'{}(\d+)'.format(prefix), key)
                if matchs is not None:
                    input_id = int(matchs.group(1))
                    if data_min <= input_id <= data_max:
                        return ranges

    def search_rand(self, value):
        type_value = None

        if isinstance(value, str):
            matchs = re.search(r"^rand:::(\w+)", value)
            if matchs is not None:
                type_value = matchs.group(1)
        return type_value

    def serialize_obj(self, param):
        return self.get_random_object(param)

    def is_any_data(self, key):
        return self.data.get(key) is not None

    def __call__(self, **kwargs):
        response = "OK"
        if len(kwargs) == 0:
            response = json.dumps(self.data, indent=4) + '\n'
            log.info("GET HTTP/1.1 %s %s 200" % (self.endpoint, self.param))
        else:
            for name, param in kwargs.items():
                obj = self.serialize_obj(param)
                if obj is None:
                    response = Response(status=404)
                    log.info("GET HTTP/1.1 %s %s 404" % (self.endpoint, param))
                else:
                    response = json.dumps(obj, indent=4) + '\n'
                    log.info("GET HTTP/1.1 %s %s 200" % (self.endpoint, param))
        return response


def search_param(endpoint):
    match = re.search(r'\<(\w+)\>', endpoint)
    if match is not None and len(match.groups()) > 0:
        return match.group(1)


def bind_routes():
    config = read_config(os.getenv('FAKEAPI_CONFIG'))

    for route in config['routes']:
        endpoint = route['endpoint']
        name = route['name']
        data = route['data']
        param = search_param(endpoint)
        app.route(endpoint)(HandleResponse(endpoint, name, data, param=param))


def read_config(path):
    config = {}

    if re.match('^(http|https).+', path):
        res = requests.get(path)
        if res.status_code == 200:
            config = yaml.safe_load(
                StringIO(res.content.decode('utf8'))
            )
        else:
            logging.fatal("Invalid URL %s" % path)
    else:
        try:
            with open(path) as f:
                config = yaml.safe_load(f)
        except IOError as err:
            logging.fatal(err)
    return config


bind_routes()


if __name__ == '__main__':
    app.run()
