# coding=utf8

from setuptools import find_packages, setup

setup(
    name='fakeapi',
    version='0.0.1',
    packages=find_packages(),
    entry_points='''
        [console_scripts]
        fakeapi-reloader=fakeapi.reloader:main
    ''',
    install_requires=[
        "Flask==1.1.2",
        "gunicorn==20.0.4",
        "PyYAML==5.3.1",
        "requests==2.24.0",
    ],
    python_requires='>=3.6',
    classifiers=[
        "Development Status :: 5 - In developpment",
        "Topic :: Software Development :: Libraries :: Python Modules",
        "License :: Apache License Version 2.0",
        "Programming Language :: Python :: 3",
    ]
)

